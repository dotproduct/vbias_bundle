#
# Simple Google Trends / Venturebias data retrieval tool
#
# - queries work queue for next available keyword
# - retrieve keyword trend data from google trends
# - upload retrieved data
# - repeat :)
#

import urllib
import time
import random
import base64
import sys
from pyGTrends import *

# general config
SERVICE_URI = "http://labs.voidsearch.com:2337/venturebias/"
WORK_QUEUE = "trends_topics"

# queries executed with random delay between min and max values
QUERY_DELAY_MIN = 60*1 		# min delay - 1 min
QUERY_DELAY_MAX = 60*2		# max delay = 2 min

if (len(sys.argv) != 4):
	print "usage : trends_retrieval.py [google username] [google password] [apikey]"
	sys.exit()
	
GOOGLE_USERNAME = sys.argv[1]
GOOGLE_PASSWORD = sys.argv[2]
APIKEY = sys.argv[3]

while True:

	# get next company name
	queueParams = urllib.urlencode({'apikey': APIKEY, 'queue': WORK_QUEUE})
	f = urllib.urlopen(SERVICE_URI + "queue/getNext?%s" % queueParams)
	companyName = f.read().strip()
	if (companyName == "Queue Empty"):
		break
	print "fetching trends for : " + companyName
	
	# retrieve trends
	try :

		trends = pyGTrends(GOOGLE_USERNAME, GOOGLE_PASSWORD)
		trends.download_report(companyName)
		data = trends.raw_data
		
		params = urllib.urlencode({'content' : base64.encodestring(data)})
		f = urllib.urlopen(SERVICE_URI + "trends/upload?apikey=" + APIKEY + "&entry=" + companyName, params)
		print f.read()

	except:
		# return entry to the back of the queue
		print "error retrieving trends data - returning entry to the queue"
		queueParams = urllib.urlencode({'apikey': APIKEY, 'queue': WORK_QUEUE, 'entry': companyName})
		f = urllib.urlopen(SERVICE_URI + "/queue/insert?%s" % queueParams)
		result = f.read()
	
	# sleep random interval
	time.sleep(random.randint(QUERY_DELAY_MIN, QUERY_DELAY_MAX))

